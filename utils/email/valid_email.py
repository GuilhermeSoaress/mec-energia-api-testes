import re

def validar_email(email):
    padrao = r'^[\w\.-]+@[\w\.-]+\.\w+$'
    
    if re.match(padrao, email):
        return True
    else:
        return False

def test_validar_email():
    assert validar_email('test@example.com') == True
    assert validar_email('invalid_email') == False