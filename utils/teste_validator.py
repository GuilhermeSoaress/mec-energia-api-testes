import pytest
from cnpj_validator_util import CnpjValidator

class TestCnpjValidator:

    @staticmethod
    def test_validate_zeros():
        cnpj = '00000000000000'
        with pytest.raises(Exception, match='CNPJ must contain exactly 14 numerical digits'):
            CnpjValidator.validate(cnpj)

    def test_validate_cnpj(self):
        cnpj = '00038174000143'
        CnpjValidator.validate(cnpj)